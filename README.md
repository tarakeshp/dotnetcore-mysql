
# Basic ASP.NET Core RESTful API
### Stack used
1.	Dotnet Core 3.1.1
2.	EF6 Core
3.	MySql (free cloud service)

## Configuration
1. Create database in mysql with anyname
2. Change the configuration in the connection string section in appsetings.json under GWE.WebApi project
3. Build the code
4. issue the command ** dotnet run **

## RESTful calls
Find the attached postman collection in the project

## Flow
1. Login as super admin (sadmin/123456) or as User ( tarakesh/123456)
2. Fetch the user list as a superadmin (200 OK)
3. Fetch the user list as a user ( 401 unauthorized
