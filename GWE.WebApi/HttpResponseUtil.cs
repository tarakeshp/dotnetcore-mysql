﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GWE.WebApi
{
    public static class HttpResponseUtil
    {
        public static IActionResult Ok(object data)
        {
            return new JsonResult(new { data = data, status = "success" });
        }

        public static IActionResult NotOk(object data)
        {
            return new JsonResult(new { data = data, status = "error" })
            {
                StatusCode = StatusCodes.Status400BadRequest
            };
        }

        public static IActionResult Unauthorized()
        {
            return new JsonResult(new { })
            {
                StatusCode = StatusCodes.Status401Unauthorized
            }; ;
        }
    }
}
