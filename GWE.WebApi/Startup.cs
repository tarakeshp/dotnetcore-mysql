using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GWE.Common;
using GWE.Persistance;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.EntityFrameworkCore;
using GWE.Persistance.Dao;
using GWE.Persistance.Models;
using GWE.Core;

namespace GWE.WebApi
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddHttpContextAccessor();
            services.AddScoped<IUserDao, UserDao>();
            services.AddScoped<IRoleDao, RoleDao>();
            services.AddScoped<IPrivilegeDao, PrivilegeDao>();
            services.AddScoped<SecurityBao, SecurityBao>();
            services.AddDbContextPool<AppDbContext>(options => options
                      // replace with your connection string
                      .UseMySql(AppConfiguration.Get.ConnectionStringSection.GetSection("MySql").Value)
                  );
            services.AddControllers();

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
