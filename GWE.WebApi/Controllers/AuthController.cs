﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GWE.Common;
using GWE.Core;
using GWE.Persistance.Dao;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace GWE.WebApi.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class AuthController : ControllerBase
    {
        private IUserDao userDao = null;
        private IRoleDao roleDao = null;
        private SecurityBao securityBao = null;
        private readonly ILogger<AuthController> _logger;
        public AuthController(SecurityBao securityBao, IUserDao userDao, IRoleDao roleDao, ILogger<AuthController> logger)
        {
            _logger = logger;
            this.securityBao = securityBao;
            this.userDao = userDao;
            this.roleDao = roleDao;
        }

        //public async Task<IActionResult> Index()
        //{
        //    var result = await this.userDao.List();
        //    return new JsonResult(result);
        //}

        [HttpPost]
        public async Task<IActionResult> Login([FromBody] LoginRequest request)
        {
            var result = await this.securityBao.ValidateCredentials(request);
            if (!result)
            {
                return HttpResponseUtil.Unauthorized();
            }

            var user = await this.userDao.Get(request.UserId);
            var sessionId = "1-0000-0000-0000-0000";
            if(user.Role.Name == "user")
            {
                sessionId = "2-0000-0000-0000-0001";
            }
            return HttpResponseUtil.Ok(new { user = user, sessionId = sessionId });
        }
    }
}