﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GWE.Core;
using GWE.Persistance.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;

namespace GWE.WebApi.Controllers
{
    public class BaseController : ControllerBase
    {
        protected string AccessToken = null;
        protected Privilege Privileges = null;
        protected SecurityBao securityBao = null;
        private Microsoft.AspNetCore.Http.IHttpContextAccessor httpContextAccessor = null;
        public BaseController(Microsoft.AspNetCore.Http.IHttpContextAccessor httpContextAccessor, SecurityBao securityBao):base()
        {
            this.securityBao = securityBao;
            this.AccessToken = httpContextAccessor.HttpContext.Request.GetHeader("x-session");
            if(this.AccessToken != null)
            {
                this.Privileges = this.securityBao.GetPrivileges(this.AccessToken.Split("-")[0]).Result;
            }
        }
    }
}