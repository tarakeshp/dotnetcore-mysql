﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GWE.Common;
using GWE.Core;
using GWE.Persistance.Dao;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace GWE.WebApi.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class UsersController : BaseController
    {
        private IUserDao userDao = null;
        private IRoleDao roleDao = null;
        private IPrivilegeDao privilegeDao = null;
        private readonly ILogger<AuthController> _logger;
        public UsersController(Microsoft.AspNetCore.Http.IHttpContextAccessor httpContextAccessor, 
            SecurityBao securityBao, IUserDao userDao, IRoleDao roleDao, IPrivilegeDao privilegeDao, ILogger<AuthController> logger):base(httpContextAccessor, securityBao)
        {
            _logger = logger;
            this.userDao = userDao;
            this.roleDao = roleDao;
            this.privilegeDao = privilegeDao;
        }

        public async Task<IActionResult> Get()
        {
            if (!this.Privileges.AllowViewUsers)
            {
                return HttpResponseUtil.Unauthorized();
            }
            var result = await this.userDao.List();
            return HttpResponseUtil.Ok(result);
        }


        [HttpGet("/profile")]
        public async Task<IActionResult> Profile()
        {
            var result = await this.userDao.List();
            return HttpResponseUtil.Ok(result);
        }
    }
}