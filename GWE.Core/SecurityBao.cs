﻿using GWE.Common;
using GWE.Persistance.Dao;
using GWE.Persistance.Models;
using System;
using System.Threading.Tasks;

namespace GWE.Core
{
    public class SecurityBao
    {
        private IUserDao userDao = null;
        private IPrivilegeDao privilegeDao = null;
        private IRoleDao roleDao = null;
        public SecurityBao(IUserDao userDao, IRoleDao roleDao, IPrivilegeDao privilegeDao)
        {
            this.userDao = userDao;
            this.roleDao = roleDao;
            this.privilegeDao = privilegeDao;
        }

        public async Task<bool> ValidateCredentials(LoginRequest request)
        {
            var user = await this.userDao.Get(request.UserId);
            if(user == null || user.Pasword != request.Password)
            {
                return false;
            }
            return true;
        }

        public async Task<Privilege> GetPrivileges(object roleId)
        {
            return await this.privilegeDao.GetByRoleId(roleId);
        }
    }
}
