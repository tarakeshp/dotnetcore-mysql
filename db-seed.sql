
INSERT INTO `Roles` VALUES (1,'sadmin','Super Administrator');
INSERT INTO `Roles` VALUES (2,'user','End user');

INSERT INTO `Privileges` VALUES (1,1,1,1,1,1,1);
INSERT INTO `Privileges` VALUES (2,2,0,0,0,0,0);


INSERT INTO `Users` VALUES (1,'Admin','Super','123456',1,'sadmin');
INSERT INTO `Users` VALUES (2,'Tarakesh','Pulikonda','123456',2,'tarakesh');