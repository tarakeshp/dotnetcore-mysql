﻿using System;
using System.Collections.Generic;
using System.Text;
using GWE.Persistance;
using GWE.Persistance.Models;

namespace GWE.Persistance.Dao
{
    public interface IUserDao : IBaseDao<User>
    {
    }
}
