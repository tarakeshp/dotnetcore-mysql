﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using GWE.Persistance;
using GWE.Persistance.Models;

namespace GWE.Persistance.Dao
{
    public interface IPrivilegeDao : IBaseDao<Privilege>
    {
        Task<Privilege> GetByRoleId(object roleId);
    }
}
