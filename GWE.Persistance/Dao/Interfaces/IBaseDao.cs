﻿using GWE.Persistance.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace GWE.Persistance
{
    public interface IBaseDao<T> where T : class
    {
        public Task<IList<T>> List();
        public Task<T> Get(object id);
        public Task Create(T item);
        public Task<object> Delete(object id);
        public Task<T> Update(object id, T item);
    }
}