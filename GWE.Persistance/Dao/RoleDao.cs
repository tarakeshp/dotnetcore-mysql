﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GWE.Persistance.Models;
using Microsoft.EntityFrameworkCore;

namespace GWE.Persistance.Dao
{
    public sealed class RoleDao : IRoleDao
    {
        private AppDbContext context = null;
        public RoleDao(AppDbContext context)
        {
            this.context = context;
        }
        public Task Create(Role item)
        {
            throw new NotImplementedException();
        }

        public Task<object> Delete(object id)
        {
            throw new NotImplementedException();
        }

        public async Task<Role> Get(object id)
        {
            return await this.context.Roles.Where(x => x.Id.Equals(Convert.ToInt32(id))).SingleOrDefaultAsync<Role>();
        }

        public Task<IList<Role>> List()
        {
            throw new NotImplementedException();
        }

        public Task<Role> Update(object id, Role item)
        {
            throw new NotImplementedException();
        }
    }
}
