﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GWE.Persistance.Models;
using Microsoft.EntityFrameworkCore;

namespace GWE.Persistance.Dao
{
    public sealed class PrivilegeDao : IPrivilegeDao
    {
        private AppDbContext context = null;
        public PrivilegeDao(AppDbContext context)
        {
            this.context = context;
        }
        public Task Create(Privilege item)
        {
            throw new NotImplementedException();
        }

        public Task<object> Delete(object id)
        {
            throw new NotImplementedException();
        }

        public async Task<Privilege> Get(object id)
        {
            return await this.context.Privileges.Where(x => x.Id.Equals(Convert.ToInt32(id))).SingleOrDefaultAsync<Privilege>();
        }

        public async Task<Privilege> GetByRoleId(object roleId)
        {
            return await this.context.Privileges.Where(x => x.RoleId.Equals(Convert.ToInt32(roleId))).SingleOrDefaultAsync<Privilege>();
        }

        public Task<IList<Privilege>> List()
        {
            throw new NotImplementedException();
        }

        public Task<Privilege> Update(object id, Privilege item)
        {
            throw new NotImplementedException();
        }
    }
}
