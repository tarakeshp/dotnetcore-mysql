﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GWE.Persistance.Models;
using Microsoft.EntityFrameworkCore;

namespace GWE.Persistance.Dao
{
    public sealed class UserDao : IUserDao
    {
        private AppDbContext context = null;
        public UserDao(AppDbContext context)
        {
            this.context = context;
        }
        public Task Create(User item)
        {
            throw new NotImplementedException();
        }

        public Task<object> Delete(object id)
        {
            throw new NotImplementedException();
        }

        public async Task<User> Get(object id)
        {
            return await this.context.Users.Where(x => x.PrincipleId.Equals(Convert.ToString(id))).Include(x=>x.Role).SingleOrDefaultAsync<User>();
        }

        public async Task<IList<User>> List()
        {
            return await this.context.Users.ToListAsync();
        }

        public Task<User> Update(object id, User item)
        {
            throw new NotImplementedException();
        }
    }
}
