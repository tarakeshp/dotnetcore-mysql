﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace GWE.Persistance.Migrations
{
    public partial class tbl_user_col_principleid : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "PrincipleId",
                table: "Users",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "PrincipleId",
                table: "Users");
        }
    }
}
