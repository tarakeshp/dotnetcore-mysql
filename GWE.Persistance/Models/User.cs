﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace GWE.Persistance.Models
{
    public class User
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }

        public string PrincipleId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Pasword { get; set; }

        public virtual Role Role { get; set; }
    }
}
