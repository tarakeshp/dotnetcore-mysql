﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace GWE.Persistance.Models
{
    public sealed class Privilege
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }
        public long RoleId { get; set; }
        public bool AllowViewUsers { get; set; }
        public bool AllowAddUser { get; set; }
        public bool AllowDeleteUser { get; set; }
        public bool AllowProfileUodate { get; set; }
        public bool AllowChangePassword { get; set; }
    }
}
